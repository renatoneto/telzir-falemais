<?php
namespace Api;

use Telzir\FaleMais\Api\ResponseContent;

class FareController extends \BaseController
{

    protected $content;
    protected $repository;

    public function __construct()
    {
        $this->content    = new ResponseContent();
        $this->repository = \App::make('FareRepositoryInterface');
    }

	public function listAction($ddd)
	{
        $this->content->setSuccess($this->repository->listByDdd($ddd));
        return \Response::make($this->content);
	}

}