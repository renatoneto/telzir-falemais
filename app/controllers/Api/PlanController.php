<?php
namespace Api;

use Telzir\FaleMais\Api\ResponseContent;
use Telzir\FaleMais\Calculator\Plan;

class PlanController extends \BaseController
{

    protected $content;
    protected $repository;

    public function __construct()
    {
        $this->content    = new ResponseContent();
        $this->repository = \App::make('PlanRepositoryInterface');
    }

	public function listAction()
	{
        $this->content->setSuccess($this->repository->all());
        return \Response::make($this->content);
	}

}