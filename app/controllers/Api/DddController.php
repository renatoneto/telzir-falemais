<?php
namespace Api;

use Telzir\FaleMais\Api\ResponseContent;
use Telzir\FaleMais\Calculator\Plan;
use Telzir\FaleMais\Calculator\Price;

class DddController extends \BaseController
{

    protected $content;
    protected $repository;

    public function __construct()
    {
        $this->content    = new ResponseContent();
        $this->repository = \App::make('DddRepositoryInterface');
    }

	public function listAction()
	{
        $this->content->setSuccess($this->repository->all());
        return \Response::make($this->content);
	}

    public function calculateAction($ddd, $dddTo, $minutes, $plan = null)
    {
        try {
            $calculator = new Price($ddd, $dddTo, \App::make('FareRepositoryInterface'));

            if ($plan !== null) {

                /**
                 * @var \Telzir\FaleMais\Repositories\PlanRepositoryInterface $planRepository
                 */
                $planRepository = \App::make('PlanRepositoryInterface');

                if (($_plan = $planRepository->getById($plan)) !== false) {
                    $plan = new Plan($_plan['free_minutes'], $_plan['fare_addition']);
                }
            }

            $withoutPlan = $calculator->calculate($minutes);
            $withPlan    = ($plan !== null) ? $calculator->calculate($minutes, $plan) : '-';

            $this->content->setSuccess([
                'priceWithoutPlan' => $withoutPlan,
                'priceWithPlan'    => $withPlan,
            ]);

        } catch (\DomainException $e) {
            $this->content->setError($e->getMessage());
        }

        return \Response::make($this->content);
    }

}