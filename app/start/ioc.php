<?php

App::bind('FareRepositoryInterface', function()
{
    return new \Telzir\FaleMais\Repositories\FareDbRepository();
});

App::bind('PlanRepositoryInterface', function()
{
    return new \Telzir\FaleMais\Repositories\PlanDbRepository();
});

App::bind('DddRepositoryInterface', function()
{
    return new \Telzir\FaleMais\Repositories\DddDbRepository();
});