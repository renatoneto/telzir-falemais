<!DOCTYPE html>
<html lang="en" ng-app="faleMaisApp">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Renato Neto">

    <title>Fale Mais :: Powered by Telzir</title>

    <link href="<?php echo asset('css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo asset('css/style.css'); ?>" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="<?php echo asset('js/angular.min.js'); ?>"></script>
    <script src="<?php echo asset('js/controllers.js'); ?>"></script>
    <script src="<?php echo asset('js/angular-locale_pt-br.js'); ?>"></script>
</head>

<body ng-controller="defaultController">

<div class="jumbotron">
    <div class="container">
        <h1>
            Fale Mais
            <small class="fsize-12">powered by Telzir</small>
        </h1>
        <p>Simule suas ligações para saber o quanto irá gastar!</p>
    </div>
</div>

<div class="container">

    <div class="row" ng-hide="!error">
        <div class="col-lg-12">
            <div class="alert alert-danger">
                {{error}}
            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-md-2">
            <h2>Origem</h2>
            <p>
                <select class="form-control" ng-model="selectedDdd" ng-options="item.ddd as '0' + item.ddd for item in ddds" ng-change="loadFares()">
                    <option value="">Selecione</option>
                </select>
            </p>
        </div>

        <div class="col-md-2">
            <h2>Destino</h2>
            <p>
                <select class="form-control" ng-model="selectedDddTo" ng-options="item.ddd_to as '0' + item.ddd_to for item in fares" ng-disabled="!selectedDdd">
                    <option value="">Selecione</option>
                </select>
            </p>
        </div>

        <div class="col-md-3">
            <h2>Plano</h2>
            <p>
                <select class="form-control" ng-model="selectedPlan" ng-options="item.id as item.name for item in plans">
                    <option value="">Selecione</option>
                </select>
            </p>
        </div>

        <div class="col-md-2">
            <h2>Minutos</h2>
            <p>
                <input type="number" class="form-control" ng-model="minutes" min="1">
            </p>
        </div>

        <div class="col-md-3">
            <h2>&nbsp;</h2>
            <p>
                <button type="button" class="btn btn-success" ng-click="simulate()">Simular</button>
            </p>
        </div>
    </div>

    <div class="row" id="box-loading" ng-hide="!loading">
        <div class="col-md-12">
            <img src="<?php echo asset('img/loading.gif'); ?>" /> Aguarde, carregando...
        </div>
    </div>

    <div class="table-responsive" id="box-result" ng-hide="!searched">

        <h2>Resultado</h2>

        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Origem</th>
                <th>Destino</th>
                <th>Tempo</th>
                <th>Plano FaleMais</th>
                <th>Com FaleMais</th>
                <th>Sem FaleMais</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td>0{{selectedDdd}}</td>
                    <td>0{{selectedDddTo}}</td>
                    <td>{{minutes}}</td>
                    <td>{{planName}}</td>
                    <td>{{priceWithPlan | currency:"R$ "}}</td>
                    <td>{{priceWithoutPlan | currency:"R$ "}}</td>
                </tr>
            </tbody>
        </table>

    </div>

    <hr>

    <footer>
        <p>&copy; Telzir 2014</p>
    </footer>
</div> <!-- /container -->


</body>
</html>
