<?php
namespace Telzir\FaleMais;

class Ddd extends \Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'ddd';

    public $timestamps = false;

}