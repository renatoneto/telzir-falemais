<?php
namespace Telzir\FaleMais;

class Fare extends \Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'fare';

    public $timestamps = false;

}