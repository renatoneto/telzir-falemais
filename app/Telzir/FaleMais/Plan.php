<?php
namespace Telzir\FaleMais;

class Plan extends \Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'plan';

    public $timestamps = false;

}