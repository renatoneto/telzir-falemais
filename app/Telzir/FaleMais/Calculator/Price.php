<?php
namespace Telzir\FaleMais\Calculator;

use Telzir\FaleMais\Repositories\FareRepositoryInterface;

class Price
{

    protected $ddd;
    protected $dddTo;
    protected $fare;

    public function __construct($ddd, $dddTo, FareRepositoryInterface $fareRepository)
    {
        $fare = $fareRepository->getByDddAndDddTo($ddd, $dddTo);

        if (!$fare) {
            throw new \DomainException('Calculo não disponível para os ddds informados');
        }

        $this->setDdd($fare['ddd']);
        $this->setDddTo($fare['ddd_to']);
        $this->setFare($fare['fare']);
    }

    public function calculate($minutes, Plan $plan = null)
    {
        $minutes = (int)$minutes;

        if ($minutes <= 0) {
            return 0;
        }

        if ($plan && $plan->getMinutes()) {

            $minutes -= $plan->getMinutes();

            if ($minutes <= 0) {
                return 0;
            }

            $total    = $minutes * $this->getFare();

            if ($addition = $plan->getAddition()) {
                $total += ($total / 100) * $addition;
            }

            return round($total, 2);
        }

        return round($minutes * $this->getFare(), 2);
    }

    public function setDdd($ddd)
    {
        $ddd = (int)$ddd;

        if (!$ddd) {
            throw new \InvalidArgumentException('DDD inválido');
        }

        $this->ddd = $ddd;
    }

    public function getDdd()
    {
        return $this->ddd;
    }

    public function setDddTo($ddd)
    {
        $ddd = (int)$ddd;

        if (!$ddd) {
            throw new \InvalidArgumentException('DDD inválido');
        }

        $this->dddTo = $ddd;
    }

    public function getDddTo()
    {
        return $this->dddTo;
    }

    public function setFare($fare)
    {
        $fare = (float)$fare;

        if (!$fare) {
            throw new \InvalidArgumentException('Tarifa inválida');
        }

        $this->fare = $fare;
    }

    public function getFare()
    {
        return $this->fare;
    }

} 