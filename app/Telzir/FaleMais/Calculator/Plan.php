<?php
namespace Telzir\FaleMais\Calculator;

class Plan
{

    protected $minutes;
    protected $addition;

    public function __construct($minutes, $addition)
    {
        $this->setMinutes($minutes);
        $this->setAddition($addition);
    }

    public function setMinutes($minutes)
    {
        $minutes = (int)$minutes;

        if (!$minutes) {
            throw new \InvalidArgumentException('Minutos inválidos');
        }

        $this->minutes = $minutes;
    }

    public function getMinutes()
    {
        return $this->minutes;
    }

    public function setAddition($addition)
    {
        $addition = (int)$addition;

        if (!$addition) {
            throw new \InvalidArgumentException('Porcentagem inválida');
        }

        $this->addition = $addition;
    }

    public function getAddition()
    {
        return $this->addition;
    }

} 