<?php
namespace Telzir\FaleMais\Repositories;

interface FareRepositoryInterface
{

    public function listByDdd($ddd);
    public function getByDddAndDddTo($ddd, $dddTo);

} 