<?php
namespace Telzir\FaleMais\Repositories;

use Telzir\FaleMais\Ddd;

class DddDbRepository implements DddRepositoryInterface
{

    public function all()
    {
        return Ddd::all()->toArray();
    }

}