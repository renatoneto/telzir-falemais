<?php
namespace Telzir\FaleMais\Repositories;

use Telzir\FaleMais\Fare;

class FareDbRepository implements FareRepositoryInterface
{

    public function listByDdd($ddd)
    {
        return Fare::where('ddd', '=', (int)$ddd)->get()->toArray();
    }

    public function getByDddAndDddTo($ddd, $dddTo)
    {
        $fare = Fare::whereRaw('ddd = ? AND ddd_to = ?', [(int)$ddd, (int)$dddTo])->first();
        return ($fare) ? $fare->toArray() : [];
    }

}