<?php
namespace Telzir\FaleMais\Repositories;

use Telzir\FaleMais\Plan;

class PlanDbRepository implements PlanRepositoryInterface
{

    public function getById($id)
    {
        $plan = Plan::where('id', '=', $id)->first();
        return ($plan) ? $plan->toArray() : false;
    }

    public function all()
    {
        return Plan::all()->toArray();
    }

}