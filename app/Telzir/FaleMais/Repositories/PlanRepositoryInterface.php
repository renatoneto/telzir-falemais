<?php
namespace Telzir\FaleMais\Repositories;

interface PlanRepositoryInterface
{

    public function getById($id);
    public function all();

} 