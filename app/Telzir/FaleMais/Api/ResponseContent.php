<?php
namespace Telzir\FaleMais\Api;

use Illuminate\Support\Contracts\JsonableInterface;

class ResponseContent implements JsonableInterface
{

    const ERROR_STATUS   = 'error';
    const SUCCESS_STATUS = 'success';

    public $status;

    /**
     * @param $message
     * @param null $data
     */
    public function setError($message, $data = null)
    {
        $this->status  = self::ERROR_STATUS;
        $this->message = $message;

        if ($data !== null) {
            $this->data = $data;
        }
    }

    /**
     * @param null $data
     */
    public function setSuccess($data = null)
    {
        $this->status = self::SUCCESS_STATUS;
        $this->data   = $data;
    }

    public function toJson($options = 0)
    {
        return json_encode($this, $options);
    }

} 