<?php

use Telzir\FaleMais\Ddd;

class DddTableSeeder extends Seeder
{

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        DB::table('ddd')->delete();

        Ddd::create(['ddd' => 11]);
        Ddd::create(['ddd' => 16]);
        Ddd::create(['ddd' => 17]);
        Ddd::create(['ddd' => 18]);
	}

}