<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('DddTableSeeder');
		$this->call('FareTableSeeder');
		$this->call('PlanTableSeeder');
	}

}