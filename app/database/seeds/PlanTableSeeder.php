<?php

use Telzir\FaleMais\Plan;

class PlanTableSeeder extends Seeder
{

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        DB::table('plan')->delete();

        Plan::create(['name' => 'FaleMais 30', 'free_minutes' => 30, 'fare_addition' => 10]);
        Plan::create(['name' => 'FaleMais 60', 'free_minutes' => 60, 'fare_addition' => 10]);
        Plan::create(['name' => 'FaleMais 120', 'free_minutes' => 120, 'fare_addition' => 10]);
	}

}