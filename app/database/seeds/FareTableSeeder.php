<?php

use Telzir\FaleMais\Fare;

class FareTableSeeder extends Seeder
{

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        DB::table('fare')->delete();

        Fare::create(['ddd' => 11, 'ddd_to' => 16, 'fare' => 1.90]);
        Fare::create(['ddd' => 16, 'ddd_to' => 11, 'fare' => 2.90]);
        Fare::create(['ddd' => 11, 'ddd_to' => 17, 'fare' => 1.70]);
        Fare::create(['ddd' => 17, 'ddd_to' => 11, 'fare' => 2.70]);
        Fare::create(['ddd' => 11, 'ddd_to' => 18, 'fare' => 0.90]);
        Fare::create(['ddd' => 18, 'ddd_to' => 11, 'fare' => 1.90]);
	}

}