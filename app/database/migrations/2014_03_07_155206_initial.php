<?php

use Illuminate\Database\Migrations\Migration;

class Initial extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('ddd', function($table)
        {
            $table->tinyInteger('ddd');
            $table->primary('ddd');
        });

        Schema::create('fare', function($table)
        {
            $table->tinyInteger('ddd');
            $table->tinyInteger('ddd_to');

            $table->primary(array('ddd', 'ddd_to'));
            $table->decimal('fare', 5, 2);

            $table->foreign('ddd')->references('ddd')->on('ddd');
            $table->foreign('ddd_to')->references('ddd')->on('ddd');
        });

        Schema::create('plan', function($table)
        {
            $table->increments('id');
            $table->string('name', 30);

            $table->tinyInteger('free_minutes');
            $table->tinyInteger('fare_addition');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('fare');
        Schema::dropIfExists('ddd');
        Schema::dropIfExists('plan');
	}

}
