<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('home');
});

Route::group(['prefix' => '/api/'], function()
{

    Route::group(['prefix' => '/ddd'], function()
    {
        Route::get('/', ['uses' => 'Api\DddController@listAction']);
    });

    Route::group(['prefix' => '/fare'], function()
    {
        Route::get('/{ddd}', ['uses' => 'Api\FareController@listAction']);
    });

    Route::group(['prefix' => '/plan'], function()
    {
        Route::get('/', ['uses' => 'Api\PlanController@listAction']);
    });

    Route::group(['prefix' => '/calculate/{ddd}/{dddTo}/{minutes}/{plan?}'], function()
    {
        Route::get('/', ['uses' => 'Api\DddController@calculateAction']);
    });

});