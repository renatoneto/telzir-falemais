<?php

use Telzir\FaleMais\Api\ResponseContent;

class ResponseContentTest extends TestCase
{

    protected $responseContent;

    public function setUp()
    {
        $this->responseContent = new ResponseContent();
        parent::setUp();
    }

	public function testErrorStatusWithoutData()
	{
        $this->responseContent->setError('test error');

        $this->assertEquals($this->responseContent->status, ResponseContent::ERROR_STATUS);
        $this->assertEquals($this->responseContent->message, 'test error');
        $this->assertTrue(!isset($this->responseContent->data));
    }

    public function testErrorStatusWithData()
    {
        $this->responseContent->setError('test error', ['test' => 'invalid']);

        $this->assertEquals($this->responseContent->status, ResponseContent::ERROR_STATUS);
        $this->assertEquals($this->responseContent->message, 'test error');
        $this->assertEquals($this->responseContent->data, ['test' => 'invalid']);
	}

	public function testSuccessStatus()
	{
        $data = ['tests' => ['test' => [true]]];

        $this->responseContent->setSuccess($data);

        $this->assertEquals($this->responseContent->status, ResponseContent::SUCCESS_STATUS);
        $this->assertEquals($this->responseContent->data, $data);
        $this->assertTrue(!isset($this->responseContent->message));
	}

	public function testToJson()
	{
        $data = ['tests' => ['test' => [true]]];

        $this->responseContent->setSuccess($data);

        $compare = [
            'status' => ResponseContent::SUCCESS_STATUS,
            'data'   => $data
        ];

        $this->assertEquals($this->responseContent->toJson(), json_encode($compare));
	}

}