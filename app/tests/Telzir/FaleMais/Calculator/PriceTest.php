<?php

use Telzir\FaleMais\Calculator\Price as PriceCalculator;
use Telzir\FaleMais\Calculator\Plan;

class PriceTest extends TestCase
{

    protected $fareRepository;
    protected $emptyFareRepository;

    public function setUp()
    {
        $this->emptyFareRepository = Mockery::mock('\Telzir\FaleMais\Repositories\FareRepositoryInterface');
        $this->emptyFareRepository->shouldReceive('getByDddAndDddTo')->once()->andReturn([]);

        $this->fareRepository = Mockery::mock('\Telzir\FaleMais\Repositories\FareRepositoryInterface');
        $this->fareRepository->shouldReceive('getByDddAndDddTo')->once()->andReturn([
            'ddd'    => 11,
            'ddd_to' => 17,
            'fare'   => '1.70'
        ]);

        parent::setUp();
    }

    /**
     * @expectedException DomainException
     * @expectedExceptionMessage Calculo não disponível para os ddds informados
     */
    public function testInstanceWithInvalidFareCombination()
    {
        new PriceCalculator(11, 17, $this->emptyFareRepository);
    }

    public function testGettersAfterInstance()
    {
        $calculator = new PriceCalculator('011', 17, $this->fareRepository);

        $this->assertEquals($calculator->getDdd(), 11);
        $this->assertEquals($calculator->getDddTo(), 17);
        $this->assertEquals($calculator->getFare(), 1.7);
    }

    public function testSetters()
    {
        $calculator = new PriceCalculator(11, 17, $this->fareRepository);

        $calculator->setDdd('016');
        $this->assertEquals($calculator->getDdd(), 16);

        $calculator->setDdd(16);
        $this->assertEquals($calculator->getDdd(), 16);

        $calculator->setDddTo('011');
        $this->assertEquals($calculator->getDddTo(), 11);

        $calculator->setDddTo(11);
        $this->assertEquals($calculator->getDddTo(), 11);

        $calculator->setFare('2.90');
        $this->assertEquals($calculator->getFare(), 2.9);

        $calculator->setFare(2.95);
        $this->assertEquals($calculator->getFare(), 2.95);
    }

    /**
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage DDD inválido
     */
    public function testSetDddWithInvalidValue()
    {
        (new PriceCalculator(11, 17, $this->fareRepository))->setDdd('invalid');
    }

    /**
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage DDD inválido
     */
    public function testSetDddToWithInvalidValue()
    {
        (new PriceCalculator(11, 17, $this->fareRepository))->setDddTo('invalid');
    }

    /**
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Tarifa inválida
     */
    public function testSetFareWithInvalidValue()
    {
        (new PriceCalculator(11, 17, $this->fareRepository))->setFare('invalid');
    }

    public function testCalculateWithoutPlan()
    {
        $calculator = new PriceCalculator(11, 17, $this->fareRepository);

        $this->assertEquals($calculator->calculate(10), 17);
        $this->assertEquals($calculator->calculate(1), 1.7);
        $this->assertEquals($calculator->calculate(100000), 170000);
        $this->assertEquals($calculator->calculate(0), 0);
        $this->assertEquals($calculator->calculate(5), 8.5);
    }

    public function testCalculateWithPlan()
    {
        $calculator = new PriceCalculator(11, 17, $this->fareRepository);

        $this->assertEquals($calculator->calculate(10, new Plan(5, 10)), 9.35);
        $this->assertEquals($calculator->calculate(1, new Plan(5, 10)), 0);
        $this->assertEquals($calculator->calculate(10000, new Plan(120, 5)), 17635.8);
        $this->assertEquals($calculator->calculate(0, new Plan(5, 5)), 0);
        $this->assertEquals($calculator->calculate(5, new Plan(4, 5)), 1.79);
        $this->assertEquals($calculator->calculate(50, new Plan(30, 200)), 102);
    }

}