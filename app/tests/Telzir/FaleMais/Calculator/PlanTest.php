<?php

use Telzir\FaleMais\Calculator\Plan;

class PlanTest extends TestCase
{

    public function testGettersAfterInstance()
    {
        $minutes  = 30;
        $addition = 10;

        $plan = new Plan($minutes, $addition);

        $this->assertEquals($plan->getMinutes(), $minutes);
        $this->assertEquals($plan->getAddition(), $addition);
    }

    public function testSetters()
    {
        $plan = new Plan(30, 10);

        $plan->setMinutes(1.5);
        $this->assertEquals($plan->getMinutes(), 1);

        $plan->setMinutes(1);
        $this->assertEquals($plan->getMinutes(), 1);

        $plan->setMinutes('10');
        $this->assertEquals($plan->getMinutes(), 10);

        $plan->setAddition('10');
        $this->assertEquals($plan->getAddition(), 10);

        $plan->setAddition(1);
        $this->assertEquals($plan->getAddition(), 1);

        $plan->setAddition(5);
        $this->assertEquals($plan->getAddition(), 5);
    }

    /**
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Minutos inválidos
     */
    public function testSetMinutesWithInvalidValue()
    {
        (new Plan(30, 10))->setMinutes('invalid');
    }

    /**
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Porcentagem inválida
     */
    public function testSetAdditionWithInvalidValue()
    {
        (new Plan(30, 10))->setAddition('invalid');
    }

}