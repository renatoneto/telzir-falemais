var faleMaisApp = angular.module('faleMaisApp', []);

faleMaisApp.controller('defaultController', function($scope, $http)
{
    $scope.selectedDdd      = '';
    $scope.selectedDddTo    = '';
    $scope.selectedPlan     = '';
    $scope.planName         = '';
    $scope.minutes          = '';
    $scope.searched         = false;
    $scope.error            = false;
    $scope.priceWithoutPlan = '';
    $scope.priceWithPlan    = '';
    $scope.loading          = false;

    // load ddd's
    $http.get('/api/ddd').success(function(response)
    {
        $scope.ddds = response.data;
    });

    // load plans
    $http.get('/api/plan').success(function(response)
    {
        $scope.plans = response.data;
    });

    $scope.loadFares = function()
    {
        if ($scope.selectedDdd) {
            $http.get('/api/fare/' + $scope.selectedDdd).success(function(response)
            {
                $scope.fares = response.data;
            });
        }
    };

    $scope.simulate = function()
    {
        validate();
        $scope.searched         = false;
        $scope.priceWithoutPlan = '';
        $scope.priceWithPlan    = '';

        if (!$scope.error && !$scope.loading) {

            $scope.loading = true;

            var url = '/api/calculate/' + $scope.selectedDdd + '/' + $scope.selectedDddTo +
                      '/' + $scope.minutes + '/' + $scope.selectedPlan;

            $scope.planName = angular.element('[ng-model="selectedPlan"]').children('option:selected').text();

            $http.get(url).success(function(response)
            {
                if (response.status == 'error') {
                    $scope.error = response.message;
                } else {

                    $scope.searched         = true;
                    $scope.priceWithoutPlan = response.data.priceWithoutPlan;
                    $scope.priceWithPlan    = response.data.priceWithPlan;

                    $scope.loading = false;

                }
            });
        }
    };

    function validate()
    {
        $scope.error = false;

        switch (true) {

            case !$scope.selectedDdd:
                $scope.error = 'Selecione o DDD de origem';
                break;

            case !$scope.selectedDddTo:
                $scope.error = 'Selecione o DDD de destino';
                break;

            case !$scope.selectedPlan:
                $scope.error = 'Selecione o plano FaleMais';
                break;

            case !$scope.minutes:
                $scope.error = 'Informe os minutos';
                break;

        }

    }
});